﻿using UnityEngine;

[System.Serializable]
public class Sound {

    public string soundName;
    public AudioClip audioClip;

    [Range(0f, 1f)]
    public float soundVolume;

    private AudioSource audioSource;


    public void SetSource(AudioSource source) {
        audioSource = source;
        source.clip = audioClip;
    }

    public void Play() {
        audioSource.volume = soundVolume;
        audioSource.Play();
    }
}

public class AudioManager : MonoBehaviour {

    [SerializeField] Sound[] sounds;


    private void Start() {
        for (int i = 0; i < sounds.Length; i++) {
            GameObject soundObject = new GameObject("Sound (" + sounds[i].soundName + ")");
            soundObject.transform.SetParent(gameObject.transform);
            sounds[i].SetSource(soundObject.AddComponent<AudioSource>());
        }

        PlaySound("Backgound Music");
    }

    public void PlaySound(string name) {
        for (int i = 0; i < sounds.Length; i++) {
            if (sounds[i].soundName == name) {
                sounds[i].Play();

                return;
            }
        }
        Debug.LogWarning("No sound named " + name + " found.");
    }
}
