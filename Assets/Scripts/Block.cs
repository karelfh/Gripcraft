﻿using UnityEngine;

public class Block : MonoBehaviour {

    [Tooltip("Is block destructible?")]
    [SerializeField] private bool destructible;
    [Tooltip("Size of array + 1 = block health.")]
    [SerializeField] private Material[] hitBlocks;

    private int timesHit = 0;


    // Method is called when block is hit by ray from PlayerController.
    private void HitByDestroyRay() {
        if (destructible) {
            timesHit++;

            int maxHits = hitBlocks.Length + 1;

            if (timesHit >= maxHits) {
                Destroy(gameObject);
            } else {
                LoadMaterial();
            }
        }     
    }

    // Loads next material from array.
    private void LoadMaterial() {
        int materialIndex = timesHit - 1;

        if (hitBlocks[materialIndex]) {
            GetComponent<Renderer>().material = hitBlocks[materialIndex];
        } else {
            Debug.LogError("Hit block material is missing!");
        }
    }

}
