﻿using UnityEngine;
using UnityEngine.UI;

public class BlockSelect : MonoBehaviour {

    [Header("UI Settings")]
    [SerializeField] private Sprite[] blockImages;
    [SerializeField] private Image blockImage;
    [SerializeField] private Text blockName;
    [SerializeField] private GameObject hotbar;
    [SerializeField] private GameObject tooltip;

    private WorldGeneration worldGen;
    private LevelManager levelManager;

    private GameObject[] blocks;
    private int activeBlock = 0;

    private bool buildModeActive;
    public bool BuildModeActive { get { return buildModeActive; } }


    private void Awake() {
        worldGen = GetComponent<WorldGeneration>();
        levelManager = FindObjectOfType<LevelManager>();

        blocks = worldGen.blocks;

        ChangeBlockImage();
        
        hotbar.SetActive(false);
        tooltip.SetActive(true);
    }

    void Update() {

        // Default: E
        if (Input.GetButtonDown("Next Block")) {
            activeBlock++;
            if (activeBlock >= blocks.Length - 1) {
                activeBlock = 0;
            }
            ChangeBlockImage();
        }

        // Default: Q
        if (Input.GetButtonDown("Previous Block")) {
            activeBlock--;
            if (activeBlock < 0) {
                activeBlock = 3;
            }
            ChangeBlockImage();
        }

        // Default: B
        if (Input.GetButtonDown("Build Menu")) {
            buildModeActive = !buildModeActive;

            if (buildModeActive) {
                tooltip.SetActive(false);
                hotbar.SetActive(true);
            }
            if (!buildModeActive) {
                tooltip.SetActive(true);
                hotbar.SetActive(false);
            }
        }

        // Default: P
        if (Input.GetButtonDown("Exit to Menu")) {
            levelManager.LoadNewLevel("Main Menu");
        }
    }

    // Return currently selected block.
    public GameObject GetBlock() {
        return blocks[activeBlock];
    }

    // Changes image in the UI based on selected block.
    private void ChangeBlockImage() {
        blockImage.sprite = blockImages[activeBlock];
        blockName.text = GetBlock().name;
    }
}
