﻿using System.Collections;
using UnityEngine;

public class ChunkGeneration : MonoBehaviour {

    private WorldGeneration worldGen;
    private GameObject[] blocks;
    private GameObject triggers;


    private void Awake() {
        worldGen = GameObject.FindGameObjectWithTag("Enviroment").GetComponent<WorldGeneration>();
        blocks = worldGen.blocks;
        triggers = worldGen.generationTriggers;
    }

    private void Start() {
        worldGen.terrainDetail = Random.Range(worldGen.terrainDetailMinValue, worldGen.terrainDetailMaxValue);
        worldGen.seed = Random.Range(0, 9999999);

        GenerateChunk(worldGen.terrainDetail, worldGen.seed);
    }

    public void GenerateChunk(float terrainDetail, float seed) {
        // Length
        for (int x = 0; x < worldGen.sizeX; x++) {
            // Width
            for (int z = 0; z < worldGen.sizeZ; z++) {

                int maxY = (int)(Mathf.PerlinNoise((x + seed) / terrainDetail, (z + seed) / terrainDetail) * worldGen.terrainHeight);
                maxY += worldGen.sizeY;

                // First layer
                GameObject grass = Instantiate(blocks[0], new Vector3(x + worldGen.xOffset, maxY, z + worldGen.zOffset), Quaternion.identity) as GameObject;
                grass.transform.SetParent(GameObject.Find("Chunk (" + worldGen.timesChunkGenerated + ")").transform);               

                // Depth
                for (int y = 0; y < maxY; y++) {
                    int dirtLayer = Random.Range(2, 5);
                    int obsidianLayer = Random.Range(6, 7);
                    int bedrockLayer = 8;

                    if (y > maxY - dirtLayer) {
                        // Second layer - Dirt
                        GameObject dirt = Instantiate(blocks[1], new Vector3(x + worldGen.xOffset, y, z + worldGen.zOffset), Quaternion.identity) as GameObject;
                        dirt.transform.SetParent(GameObject.Find("Chunk (" + worldGen.timesChunkGenerated + ")").transform);
                    } else if (y > maxY - obsidianLayer) {
                        // Third layer - Stone
                        GameObject stone = Instantiate(blocks[2], new Vector3(x + worldGen.xOffset, y, z + worldGen.zOffset), Quaternion.identity) as GameObject;
                        stone.transform.SetParent(GameObject.Find("Chunk (" + worldGen.timesChunkGenerated + ")").transform);
                    } else if (y > maxY - bedrockLayer) {
                        // Fourth layer - Obsidian
                        GameObject obsidian = Instantiate(blocks[3], new Vector3(x + worldGen.xOffset, y, z + worldGen.zOffset), Quaternion.identity) as GameObject;
                        obsidian.transform.SetParent(GameObject.Find("Chunk (" + worldGen.timesChunkGenerated + ")").transform);
                    } else {
                        // Last layer - Bedrock
                        GameObject bedrock = Instantiate(blocks[4], new Vector3(x + worldGen.xOffset, y, z + worldGen.zOffset), Quaternion.identity) as GameObject;
                        bedrock.transform.SetParent(GameObject.Find("Chunk (" + worldGen.timesChunkGenerated + ")").transform);
                    }                   

                    if (x == 0 && y == worldGen.sizeY && z == 0) {
                        // Generation triggers
                        GameObject newTriggers = Instantiate(triggers, new Vector3(x + worldGen.xOffset, maxY, z + worldGen.zOffset), Quaternion.identity) as GameObject;
                        newTriggers.transform.SetParent(GameObject.Find("Chunk (" + worldGen.timesChunkGenerated + ")").transform);

                        StartCoroutine(SetActiveAfterTime(newTriggers, true, 3f));               
                    }
                }

                if (x == (worldGen.sizeX / 2) && z == (worldGen.sizeZ / 2)) {
                    // Game over trigger
                    GameObject newGameOver = Instantiate(worldGen.gameOver, new Vector3(x + worldGen.xOffset, -maxY, z + worldGen.zOffset), Quaternion.identity) as GameObject;
                    newGameOver.transform.SetParent(GameObject.Find("Chunk (" + worldGen.timesChunkGenerated + ")").transform);
                }
                
            }
        }
        worldGen.isWorldGenerated = true;
    }

    IEnumerator SetActiveAfterTime(GameObject gameObject, bool active, float time) {
        yield return new WaitForSeconds(time);

        gameObject.SetActive(active);
    }

}
