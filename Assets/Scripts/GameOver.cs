﻿using UnityEngine;

public class GameOver : MonoBehaviour {

    private void OnTriggerEnter(Collider other) {
        if (other.gameObject.CompareTag("Player")) {
            FindObjectOfType<LevelManager>().LoadNewLevel("Main Menu");
        }
    }
}
