﻿using UnityEngine;

public class GenerationTrigger : MonoBehaviour {

    private WorldGeneration worldGen;


    private void Awake() {
        worldGen = GameObject.FindGameObjectWithTag("Enviroment").GetComponent<WorldGeneration>();
    }

    public void OnTriggerExit(Collider other) {
        if (other.gameObject.tag == "Player") {
            if (gameObject.tag == "North") {
                worldGen.xOffset += 0;
                worldGen.zOffset += 32;
                worldGen.GenerateNextChunk(worldGen.xOffset, worldGen.zOffset);

            } else if (gameObject.tag == "South") {
                worldGen.xOffset -= 0;
                worldGen.zOffset -= 32;
                worldGen.GenerateNextChunk(worldGen.xOffset, worldGen.zOffset);

            } else if (gameObject.tag == "West") {
                worldGen.xOffset -= 32;
                worldGen.zOffset -= 0;
                worldGen.GenerateNextChunk(worldGen.xOffset, worldGen.zOffset);

            } else if (gameObject.tag == "East") {
                worldGen.xOffset += 32;
                worldGen.zOffset += 0;
                worldGen.GenerateNextChunk(worldGen.xOffset, worldGen.zOffset);

            }
            Destroy(gameObject, 1f);
            worldGen.RemoveChunk();
        }
    }

}
