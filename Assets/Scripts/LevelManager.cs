﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelManager : MonoBehaviour {

    [HideInInspector] public bool loadedGame = false;


    private void Update() {
        if (GetActiveScene().name == "Game") {
            Cursor.lockState = CursorLockMode.Locked;
            Cursor.visible = false;
            loadedGame = false;
        } else {
            Cursor.lockState = CursorLockMode.None;
            Cursor.visible = true;
        }

        if (GetActiveScene().buildIndex == 0) {
            LoadNextLevel();
        }
    }

    public Scene GetActiveScene() {
        Scene scene = SceneManager.GetActiveScene();
        return scene;
    }

    public void LoadNewLevelAndLoadData(string levelName) {
        loadedGame = true;
        SceneManager.LoadScene(levelName);
    }

    public void LoadNewLevel(string levelName) {
        SceneManager.LoadScene(levelName);
    }

    public void LoadNextLevel() {
        SceneManager.LoadScene(GetActiveScene().buildIndex + 1);
    }

	public void QuitGame() {
        Application.Quit();
    }
}
