﻿using UnityEngine;

public class PlayerController : MonoBehaviour {

    [Header("Movement Settings")]
    [SerializeField] private float speed = 5f;
    [SerializeField] private float lookSensitivity = 3f;
    [SerializeField] private float jumpForce = 1000f;
    [SerializeField] private float groundDistance = 1.2f;

    [Header("Interaction Settings")]
    [SerializeField] private float interactionDistance;
    [SerializeField] private float interactionRate;
    [SerializeField] private GameObject wireframeBlock;
    [SerializeField] private Camera cam;
    [SerializeField] private GameObject mask;

    private BlockSelect blockSelect;
    private PlayerMotor playerMotor;
    private float nextInteraction;


    private void Awake() {
        playerMotor = GetComponent<PlayerMotor>();
        blockSelect = FindObjectOfType<BlockSelect>();
    }

    private void Start() {
        // Create stencil mask infront of camera.
        GameObject newMask = Instantiate(mask, mask.transform.position, mask.transform.rotation) as GameObject;
        newMask.transform.SetParent(GameObject.FindGameObjectWithTag("MainCamera").transform);
        newMask.transform.localPosition = new Vector3(0, 0, .051f);
    }

    private void Update() {
        Interaction();
        Movement();
    }

    private void Movement() {
        float horizontal = Input.GetAxis("Horizontal");
        float vertical = Input.GetAxis("Vertical");
        float rotationX = Input.GetAxisRaw("Mouse X");
        float rotationY = Input.GetAxisRaw("Mouse Y");
        float cameraRotationX = rotationY * lookSensitivity;

        Vector3 movHorizontal = transform.right * horizontal;
        Vector3 movVertical = transform.forward * vertical;
        Vector3 velocity = (movHorizontal + movVertical) * speed;
        Vector3 rotation = new Vector3(0f, rotationX, 0f) * lookSensitivity;
        Vector3 finalJumpForce = Vector3.zero;

        if (Input.GetButtonDown("Jump")) {
            RaycastHit hit;

            // Check if player is on the ground
            if (Physics.Raycast(transform.position, Vector3.down, out hit, groundDistance)) {
                if (hit.collider.gameObject.CompareTag("Block")) {
                    finalJumpForce = Vector3.up * jumpForce;
                }
            }  
        }

        playerMotor.Move(velocity);
        playerMotor.Rotate(rotation);
        playerMotor.RotateCamera(cameraRotationX);
        playerMotor.ApplyJump(finalJumpForce);
    }

    private void Interaction() {
        // Destroy blocks.
        if (Input.GetButtonDown("Fire1") && Time.time > nextInteraction) {
            nextInteraction = Time.time + interactionRate;

            RaycastHit hit;

            if (Physics.Raycast(cam.transform.position, cam.transform.forward, out hit, interactionDistance)) {
                if (hit.collider.gameObject.CompareTag("Block")) {
                    hit.transform.SendMessage("HitByDestroyRay");
                }
            }
        }

        // Place blocks.
        if (Input.GetButtonDown("Fire2") && blockSelect.BuildModeActive && Time.time > nextInteraction) {
            nextInteraction = Time.time + interactionRate;

            RaycastHit hit;

            if (Physics.Raycast(cam.transform.position, cam.transform.forward, out hit, interactionDistance)) {
                if (hit.collider.gameObject.CompareTag("Block")) {
                    GameObject newBlock = Instantiate(blockSelect.GetBlock(), BlockPosition(hit), Quaternion.identity) as GameObject;
                    newBlock.transform.SetParent(GameObject.FindGameObjectWithTag("Chunk").transform);
                }
            }
        }

        // Wireframe block preview.
        if (blockSelect.BuildModeActive) {
            RaycastHit hit;

            if (Physics.Raycast(cam.transform.position, cam.transform.forward, out hit, interactionDistance)) {
                if (hit.collider.gameObject.CompareTag("Block")) {
                    GameObject wireframe = Instantiate(wireframeBlock, BlockPosition(hit), Quaternion.identity) as GameObject;
                    wireframe.transform.SetParent(GameObject.FindGameObjectWithTag("Enviroment").transform);
                    wireframeBlock.GetComponent<MeshRenderer>().enabled = true;
                    Destroy(wireframe.gameObject, .05f);
                }
            }

        } else {
            wireframeBlock.GetComponent<MeshRenderer>().enabled = false;
        }
    }

    private Vector3 BlockPosition(RaycastHit hit) {
        Vector3 blockPosition = hit.point + (hit.normal / 2f);

        blockPosition.x = Mathf.Round(blockPosition.x);
        blockPosition.y = Mathf.Round(blockPosition.y);
        blockPosition.z = Mathf.Round(blockPosition.z);

        return blockPosition;
    }

}
