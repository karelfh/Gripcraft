﻿using UnityEngine;

public class WorldGeneration : MonoBehaviour {

    [Header("Chunk Size Settings")]
    [Range(1, 32)]
    public int sizeX;
    [Range(1, 12)]
    public int sizeY;
    [Range(1, 32)]
    public int sizeZ;

    [Header("Chunk Terrain Settings")]
    [Range(1, 12)]
    public float terrainHeight;
    [Range(5, 15)]
    public float terrainDetailMinValue;
    [Range(5, 15)]
    public float terrainDetailMaxValue;

    [Header("General Settings")]
    public GameObject chunk;
    public GameObject player;
    public GameObject[] blocks;
    public GameObject generationTriggers;
    public GameObject gameOver;

    [HideInInspector] public float terrainDetail;
    [HideInInspector] public bool isWorldGenerated = false;
    [HideInInspector] public int timesChunkGenerated = 0;
    [HideInInspector] public int seed;
    [HideInInspector] public float xOffset, zOffset;

    private LevelManager levelManager;


    private void Start() {
        levelManager = FindObjectOfType<LevelManager>();

        if (!levelManager.loadedGame) {
            InitialWorldGeneration();
        }  
    }

    // Generate first chunk and spawn player when game is first loaded.
    private void InitialWorldGeneration() {
        GameObject newChunk = Instantiate(chunk, Vector3.zero, Quaternion.identity) as GameObject;
        timesChunkGenerated++;

        newChunk.transform.SetParent(GameObject.FindGameObjectWithTag("Enviroment").transform);
        newChunk.gameObject.name = "Chunk (" + timesChunkGenerated + ")";

        Instantiate(player, new Vector3(sizeX / 2, sizeY * 2, sizeZ / 2), Quaternion.identity);
    }

    // Generate next chunk based on x and z offest values.
    public void GenerateNextChunk(float xOffset, float zOffset) {
        GameObject newChunk = Instantiate(chunk, new Vector3(xOffset, 0, zOffset), Quaternion.identity) as GameObject;
        timesChunkGenerated++;

        newChunk.transform.SetParent(GameObject.FindGameObjectWithTag("Enviroment").transform);
        newChunk.gameObject.name = "Chunk (" + timesChunkGenerated + ")";
    }

    // Remove chunk when new one is generated
    public void RemoveChunk() {
        GameObject chunkToDestroy = GameObject.Find("Chunk (" + (timesChunkGenerated - 1) + ")");

        Destroy(chunkToDestroy, 5f);
    }

}
